Rails.application.routes.draw do
  get 'points/create'

  get 'points/history'
  
  get 'points/totaluser'
  
  get 'points/global'
  
  get 'points/country'

  post 'register', to: 'users#register'
  post 'login', to: 'users#login'

  resources :users, only: [] do
    resources :points, only: [:create, :index, :totaluser, :global, :country]
  end
end
