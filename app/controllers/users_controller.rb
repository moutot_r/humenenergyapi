class UsersController < ApplicationController

  def register
    user = User.new(register_params)

    if user.save
      render json: user
    else
      render json: { errors: user.errors }
    end
  end

  def login
    user = User.find_by(login_params)

    if user
      render json: user
    else
      status 404
      render json: { errors: [ 'user not found' ] }
    end
  end

  private

  def register_params
    params.permit(
      :social_id,
      :social_name,
      :name,
      :email,
      :location)
  end

  def login_params
    params.permit(
      :social_id,
      :social_name)
  end
end
