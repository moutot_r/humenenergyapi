class PointsController < ApplicationController

  def create
    @user = User.find(params[:user_id])
    point = @user.points.build(point_params)

    if point.save
      render json: { success: :ok }, status: 201
    else
      render json: { errors: point.errors }, status: 422
    end
  end

   def totaluser
      ptotaluser = Point.where(user_id: params[:user_id])
      render json: {"totaluser" => ptotaluser.sum(:earned_points)}
   end
  
   def global
     pglobal = Point.all
     render json: {"global" => pglobal.sum(:earned_points)}
   end
  
   def country
     pcountry = Point.joins(:user).where(users: {location: params[:country]})
     render json:{params[:country] => pcountry.sum(:earned_points)}
   end

  def index
    @user = User.find(params[:user_id])
    points = @user.points.order(:activity_date)
    render json: points
  end

  private


  def point_params
    params.permit(
      :activity_type,
      :activity_kind,
      :durations_s,
      :earned_points,
      :activity_date)
  end
end
