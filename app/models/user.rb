class User < ActiveRecord::Base
  validates :name, presence: true
  validates :email, presence: true
  validates :location, presence: true
  has_many :points
end
