class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.string :actitivy_type, null: false
      t.string :activity_kind, null: false
      t.integer :durations_s, null: false
      t.integer :earned_points, null: false
      t.datetime :activity_date, null: false
      t.timestamps null: false
    end
  end
end
