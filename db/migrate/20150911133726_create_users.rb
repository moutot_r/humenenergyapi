class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :social_id
      t.string :social_name
      t.string :name, null: false
      t.string :location, null: false
      t.string :email, null: false

      t.timestamps null: false
    end
  end
end
