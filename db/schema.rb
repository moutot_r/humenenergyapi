# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150911135800) do

  create_table "points", force: :cascade do |t|
    t.string   "activity_type", null: false
    t.string   "activity_kind", null: false
    t.integer  "durations_s",   null: false
    t.integer  "earned_points", null: false
    t.datetime "activity_date", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  add_index "points", ["user_id"], name: "index_points_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "social_id"
    t.string   "social_name"
    t.string   "name",        null: false
    t.string   "location",    null: false
    t.string   "email",       null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
